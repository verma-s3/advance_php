<?php

ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

$dbh = new PDO('sqlite:database.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$query = "SELECT * FROM users";
$params = [];
$stmt = $dbh->prepare($query);
$stmt->execute($params);
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

header('Content-Type: application/json');
echo json_encode($results);  

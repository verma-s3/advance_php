<?php

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);


header('content-type: application/json');
/*
in order for POST submission to appear in $_POST, it must be submitted as x-www-form-urlencoded:
name=steve&email=edu@pagerange.com&phone=1234567891

if POST subission   
placed in php://input as a text 
 */

// we nee dto use this dtat in PHP, eg inserting in a databse......... we must convert to a varible
$post =  json_decode(file_get_contents('php://input'));

// to send data back to Javascript, we need to convert it to string.
echo json_encode($post);

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
  <title>Ajax Database Search1</title>
  <style type="text/css">
  	#serach-container{
  		width: 300px;
  		float: right;
  		position: relative;
  	}
  </style>
</head>
<body>
  
  <div id="container">
  	<h1 >Ajax Database Search</h1>
  	<div id="serach-container">
	  	<form id="serach">
	    <input type="text" name="s" id="s">&nbsp;
	    <button>search</button> 
	    <div id="results">
	    </div>
	  </form>
	  <div id="results"></div>
	</div>
  </div>
  <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
    	$('#s').keyup(function(e){
    		axios.get('search2.php?',{
    			params: {
    				s: $(this).val()
    			}
    		})
    			.then((response) =>{
    				return response.data;
    			})
    			.then((data)=>{
    				$('#results').html(data);
    			})
    			.catch((error)=>{
    				console.log(error);
    			})
    	});
    });
  </script> 
</body>
</html>
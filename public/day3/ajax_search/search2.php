<?php

ini_set('display_errors',1);
ini_set('error_reporting',E_ALL);

$dbh = new PDO('sqlite:'.__DIR__.'/../database2.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


if(!empty($_GET['s'])){
	$query = 'SELECT * from catalog where title LIKE :s';
	$params = array(':s'=> "%".$_GET["s"]."%");
	$stmt = $dbh->prepare($query);
	$stmt->execute($params);
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if(count($results)){
		echo "<ul>";
		foreach ($results as $book) {
			echo "<li data-book_id='{$book['book_id']}''>{$book['title']}</li>";
		}
		echo "</ul>";

	}
}
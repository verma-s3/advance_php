 <?php 

//functions
//
/**
 * Escape string for output to HTML
 * @param  string $string 
 * @return string         
 */
function esc($string)
{
	
	  return htmlentities($string, null, "UTF-8");
  
}

/**
 * Escape string for output to HTML attributes
 * @param  string $string 
 * @return string         
 */
function esc_attr($string)
{
  return htmlentities($string, ENT_QUOTES, "UTF-8");
  
}

/**
 * Dump and Die
 * @param  Mixed $var 
 * @return void
 */
function dd($var){
  var_dump($var);
}

/**
 * return sanitized POST values
 * @param  String $field $_Post field name
 * @return String  the sanitized string
 */
function clean($field){
	if(!empty($_POST[$field]))
	{
		return esc_attr($_POST[$field]);
	}
	else{
		return "";
	}
}

function loadContent(json){
  // for(i=0;i<json.length; i++){
  //   document.getElementById('content').innerHTML = json[i].title;
  //   document.getElementById('content').innerHTML += json[i].author;
  //   document.getElementById('content').innerHTML += json[i].num_pages;
  //   document.getElementById('content').innerHTML += json[i].price;
  // }
  var html = '';
  for(var i in json){
    var book = json[i];
    var template = 
    `<ul>
        <li><strong>Title: </strong><a data-book_id="${book.book_id}" href="#">${book.title}</a></li>
        <li><strong>Author: </strong>${book.author}</li>
        <li><strong>Pages: </strong>${book.num_pages}</li>
        <li><strong>Price: </strong>${book.price}</li>
        <li><strong>In-Print: </strong>${(book.in_print == 1)?'yes':'no'}</li>
    </ul>`;
    html += template;
  }
  document.getElementById('content').innerHTML = html;
  addClickHandlers();
}

function addClickHandlers()
{
  var links = document.getElementsByTagName('a');
  for(var i=0;i<links.length;i++){
    links[i].onclick = function(e){
      var book_id = this.getAttribute('data-book_id');
      loadBook(book_id);
    }
  }
}

function loadBook(book_id){
  var url = 'server.php?book_id=' + book_id;
  var xhr = new XMLHttpRequest;
  xhr.open('GET',url);
  xhr.onreadystatechange = function(){
    if(xhr.readyState == 4 && xhr.status == 200){
      var json = JSON.parse(xhr.responseText);
      showBook(json);
    }
  }
  xhr.send(null);
}

function showBook(book){
  console.log(book);
  var html ="";
  var template = `
  <h2>${book.title}</h2>
  <p>
  <strong>Title: </strong>${book.title}<br />
  <strong>Author: </strong>${book.author}<br />
  <strong>Author country: </strong>${book.authhor_country}<br />
  <strong>Numbers: </strong>${book.num_pages}<br />
  <strong>Format: </strong>${book.format}<br />
  <strong>Genre: </strong>${book.genre}<br />
  <strong>Price: </strong>${book.price}<br />
  <strong>In Print: </strong>${(book.in_print == 1)?'Yes':'No'}<br />
  <strong>Publisher: </strong>${book.publisher}<br />
  <strong>Publisher city: </strong>${book.publisher_city}<br />
  <strong>Publisher phone: </strong>${book.publisher_phone}<br />
  </p>`
  ;
  html += template;
  document.getElementById('details').innerHTML = html;
}
export{loadContent, loadBook, showBook, addClickHandlers};
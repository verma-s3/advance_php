<?php

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);



$dbh = new PDO('sqlite:'.__DIR__.'/../database2.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


if(isset($_GET['book_id'])){
	$query = 'SELECT * from catalog WHERE book_id = :book_id';
	$params = array(':book_id'=>$_GET['book_id']);
	$stmt = $dbh->prepare($query);
	$stmt->execute($params);
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
}else{
	$query = 'SELECT * from catalog';
	$stmt = $dbh->prepare($query);
	$stmt->execute();
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);	
}
//var_dump($result);
header('content-type: application/json');
echo json_encode($result);

?>
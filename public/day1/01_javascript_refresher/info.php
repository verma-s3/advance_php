<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>Javascript Refresher</title>
  <script type="text/javascript">
  	var a;
  	let b = 6;
  	const c = 'hello';

  	window.onload = function(e){
  		// document.getElementById('content').innerHTML = c;
  		// by query selector
  		document.querySelector('#content').innerHTML = c;
  	}

  	//arrays
  	let users = ['Dave','Mike','Tom', 'Romana'];

  	//Objects
  	let jobs = {
  		id: '1',
  		name: 'Programmer'
  	}

  	//Raw(Vanilla) Javascript equivalent of foreach
  	for(var i in users){
  		console.log(users[i]);
  	}


  	console.log(a);
  	console.log(b);
  	console.log(c);
  </script>
</head>
<body>
	<h1>Javascript Refresher</h1>
	<p>View console to see output</p>
	<div id="content"></div>
</body>
</html>
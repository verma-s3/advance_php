<?php

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);


$dbh = new PDO('sqlite:'.__DIR__.'/../user.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if('POST'== $_SERVER['REQUEST_METHOD']){
	//making query
	$query = "INSERT INTO form (name, email, phone) 
			  VALUES (:name, :email, :phone)";
	// parametres array
	$params = array(':name' => $_POST['name'],
					':email'=> $_POST['email'],
					':phone'=> $_POST['phone']);
	//preparing query
	$stmt = $dbh->prepare($query);
	//executing query
	$stmt->execute($params);
	//getting last insert id
	$id = $dbh->lastInsertId();

	// making query to show submitted data
	$query1 = "SELECT * FROM form where id = :id";
	// parametres array
	$params1 = array(':id' => $id);
	//preparing query
	$stmt = $dbh->prepare($query1);
	//executing query
	$stmt->execute($params1);
	//fetching data
	$result1 = $stmt->fetch(PDO::FETCH_ASSOC);
	
}

// encding data
echo json_encode($result1);

?>


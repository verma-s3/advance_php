<?php

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// databse connection
$dbh = new PDO('sqlite:'.__DIR__.'/../user.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// making query
$query = 'SELECT * FROM form';
// parameters array
$params = array();
// preparing query
$stmt = $dbh->prepare($query);
//executing array
$stmt->execute($params);
// fetching data
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
// including the user_ list php page
//var_dump($result);
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>User Infromation</title>
</head>
<body>
	<h1 >User Information</h1>
	<!-- displaying user infromation in table form -->
	<table class="table is-hoverable is-fullwidth is-striped">
	<!-- heading of user infromation -->
	<tr>	
		<th>S no.</th>
		<th>Name</th>
		<th>Email</th>
		<th>Phone</th>
		
	</tr>
	<!-- foreach starting for displaying user list -->
	<?php foreach ($result as $user): ?>
	<tr>
		<td><?=htmlentities($user['id'])?></td>
		<td><?=htmlentities($user['name'])?></td>
		<td><?=htmlentities($user['email'])?></td>
		<td><?=htmlentities($user['phone'])?></td>
	</tr>
	<?php endforeach; ?>
	<!-- end of for each loop -->
	</table>
</body>
</html>
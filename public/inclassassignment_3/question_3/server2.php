<?php

ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// databse connection
$dbh = new PDO('sqlite:'.__DIR__.'/../user.sqlite');
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if($_GET['id']){
	// making query
	$query = 'SELECT * FROM form where id = :id';
	// parameters array
	$params = array(':id' => $_GET['id']);
	// preparing query
	$stmt = $dbh->prepare($query);
	//executing array
	$stmt->execute($params);
	// fetching data
	$user = $stmt->fetch(PDO::FETCH_ASSOC);
	// including the user_ list php page
}
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0" />
  <meta name="description" content="" />
  <title>User Infromation</title>
</head>
<body>
	<h1 >User Detail Data</h1>
	<!-- displaying user infromation in table form -->
	<table class="table is-hoverable is-narrow is-striped">
	<!-- heading of user infromation -->
	<tr>
		<th>Name</th>
		<th>Email</th>
		<th>Phone</th>
		
	</tr>
	<!-- foreach starting for displaying user list -->
	<tr>
		<td><?=htmlentities($user['name'])?></a></td>
		<td><?=htmlentities($user['email'])?></td>
		<td><?=htmlentities($user['phone'])?></td>
	</tr>
	<!-- end of for each loop -->
	</table>
</body>
</html>